Source: python-twitter
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Koichi Akabe <vbkaisetsu@gmail.com>
Build-Depends: debhelper-compat (= 12), dh-python, python3 (>= 3.5),
               python3-pytest-runner, python3-pytest,
               python3-responses, python3-setuptools,
               python3-requests-oauthlib, python3-sphinx,
               python3-sphinx-rtd-theme
Standards-Version: 4.1.1
Homepage: https://github.com/bear/python-twitter
Vcs-Git: https://salsa.debian.org/python-team/packages/python-twitter.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-twitter
Testsuite: autopkgtest-pkg-python

Package: python3-twitter
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Suggests: www-browser, python-twitter-doc
Description: Twitter API wrapper for Python 3
 This library provides a pure Python 3 interface for the Twitter API.
 .
 Twitter provides a service that allows people to connect via the web, IM, and
 SMS. Twitter exposes a web services API (http://twitter.com/help/api) and this
 library is intended to make it even easier for Python programmers to use.

Package: python-twitter-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Replaces: python-twitter (<< 1.1+git20131227-1)
Breaks: python-twitter (<< 1.1+git20131227-1)
Description: Twitter API wrapper for Python: documentation files
 This library provides a pure Python interface for the Twitter API.
 .
 Twitter provides a service that allows people to connect via the web, IM, and
 SMS. Twitter exposes a web services API (http://twitter.com/help/api) and this
 library is intended to make it even easier for Python programmers to use.
 .
 This package contains documentation files.
